#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

cp -rfv sdlclMakefile sdlcl/Makefile
pushd "sdlcl"
make
popd
cp -rfv "sdlcl/libSDL-1.2.so.0" "3970/dist/"

cp -rfv linuxofficial "3970/dist/"
cp -rfv run-prey.sh "3970/dist/run-prey.sh"
