#!/bin/sh

# Favor system-installed xdg-open over our prepackaged copy...
export PATH="$PATH:."
exec xdg-open "$@"

